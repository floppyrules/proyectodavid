<?php  
//Fichero models/blogModel.php

class Blog{

	public $entradas; //Sera un VECTOR de Posts

	public function __construct(){
		$this->entradas=[]; //Le digo que va a ser un VECTOR vacio
	}

	public function dimeEntradas(){
		global $conexion; //Hago alusion a la conexion GLOBAL
		$sql="SELECT * FROM blog ORDER BY fecha DESC";
		//$consulta=$GLOBALS['conexion']->query($sql); //POR PROBAR
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas; //Devuelvo un ARRAY de POSTS
	}

	public function dimeEntrada($id){
		global $conexion; //Hago alusion a la conexion GLOBAL
		$sql="SELECT * FROM blog WHERE id=$id";
		//$consulta=$GLOBALS['conexion']->query($sql); //POR PROBAR
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$entrada=new Post($registro);
		return $entrada; //Devuelvo un solo POST
	}

} //Fin de la class Blog
?>