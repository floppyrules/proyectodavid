<?php  
//Fichero models/restauranteModel.php

class Restaurante{
	
	public $id;
	public $nombre;
	public $direccion;
	public $telefono;
	public $descripcion;
	public $url;

	// public $cp;
	// public $localidad;
	public $email;
	public $longitud;
	public $latitud;
	public $imagen;

	public function __construct($elemento){
		
		$this->id=$elemento->id;
		$this->nombre=$elemento->title;
		//$this->direccion=$elemento->address;
		// $this->direccion=$elemento->address->{'street-address'};
		// $this->cp=$elemento->address->{'postal-code'};
		@$this->localidad=$elemento->addressLocality;
		// @$this->longitud=$elemento->location->longitude;
		// @$this->latitud=$elemento->location->latitude;
		@$this->telefono=$elemento->tel->tel;
		@$this->descripcion=$elemento->comment;
		@$this->url=$elemento->url;
		@$this->email=$elemento->email;
		@$this->direccion=$elemento->streetAddress;

		@$utm = new UTMRef($elemento->geometry->coordinates[0], $elemento->geometry->coordinates[1], "T", 30);
    	$ll = $utm->toLatLng();

		@$this->longitud=$ll->lng;
		@$this->latitud=$ll->lat;
		@$this->imagen=$elemento->image;
	}
} //Fin de la class Restaurante

?>